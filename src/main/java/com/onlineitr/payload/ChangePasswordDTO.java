package com.onlineitr.payload;

public class ChangePasswordDTO {

	private String password;
	private String changePassword;
	private String panCardNo;
	private String currentPassword;
	private String userId;
	private Boolean isUserFirstTime;
	

	public String getPassword() {
		return password;
	}

	public String getChangePassword() {
		return changePassword;
	}

	public String getPanCardNo() {
		return panCardNo;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public String getUserId() {
		return userId;
	}

	public Boolean getIsUserFirstTime() {
		return isUserFirstTime;
	}
	
}


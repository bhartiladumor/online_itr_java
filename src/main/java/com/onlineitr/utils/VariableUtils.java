package com.onlineitr.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class VariableUtils {

	public final static ObjectMapper MAPPER = new ObjectMapper();
	public final static String TBL_USER = "tblUser";
	public final static String TBL_DOCUMENT = "tblDocument";
	public final static String TBL_USER_ROLE = "tblUserRole";

	public final static String ROLE_USER = "ROLE_USER";
	public final static String ROLE_ADMIN = "ROLE_ADMIN";

	public final static String SMS_API_KEY = "93f50d5e-4d04-11e8-a895-0200cd936042";

}

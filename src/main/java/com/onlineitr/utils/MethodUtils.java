package com.onlineitr.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.springframework.web.multipart.MultipartFile;

public class MethodUtils {
	
	private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	@SuppressWarnings("null")
	public static boolean isObjectNullOrEmpty(String param) {
		if (param != null || !param.toString().isEmpty()) {
			return false;
		}
		return true;
	}

	public static <T> boolean isSetNullOrEmpty(Set<T> set) {
		if (set == null || set.isEmpty() || set.size() == 0) {
			return true;
		}
		return false;
	}

	public static <T> boolean isListNullOrEmpty(java.util.List<T> list) {
		if (list == null || list.isEmpty() || list.size() == 0) {
			return true;
		}
		return false;
	}

	public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
		System.out.println(zipEntry.getName());
		File destFile = new File(destinationDir, zipEntry.getName());

		String destDirPath = destinationDir.getCanonicalPath();
		String destFilePath = destFile.getCanonicalPath();
		System.out.println(destDirPath);
		System.out.println(destFilePath);
		System.out.println(destDirPath + File.separator);

//        if (!destFilePath.startsWith(destDirPath + File.separator)) {
//            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
//        }

		return destFile;
	}

	public static File convertMultipartToFile(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public static boolean extractZipFile(MultipartFile file, File destDir) {
		try {

			System.err.println("111111111111111111111111111");
			byte[] buffer = new byte[1024];
			ZipInputStream zis = new ZipInputStream(new FileInputStream(MethodUtils.convertMultipartToFile(file)));
			ZipEntry zipEntry = zis.getNextEntry();
			while (zipEntry != null) {
				File newFile = MethodUtils.newFile(destDir, zipEntry);
				System.err.println(newFile.getAbsolutePath());
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				zipEntry = zis.getNextEntry();
			}
			zis.closeEntry();
			zis.close();
			System.err.println("22222222222222222222222222");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static String randomAlphaNumeric(int count) {
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
			builder.append(ALPHA_NUMERIC_STRING.charAt(character));
		}
		return builder.toString();
	}
}

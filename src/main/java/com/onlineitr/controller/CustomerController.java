package com.onlineitr.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.onlineitr.exception.ApiResponse;
import com.onlineitr.exception.BadRequestException;
import com.onlineitr.model.Documents;
import com.onlineitr.model.User;
import com.onlineitr.payload.ChangePasswordDTO;
import com.onlineitr.payload.CustomerDTO;
import com.onlineitr.payload.MyInputRequest;
import com.onlineitr.repository.DocumentRepository;
import com.onlineitr.repository.UserRepository;
import com.onlineitr.service.CustomerService;
import com.onlineitr.utils.MethodUtils;
import com.onlineitr.utils.VariableUtils;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@Autowired
	private UserRepository UserRepository;

	@Autowired
	private DocumentRepository documentRepository;

	
	@CrossOrigin
	@GetMapping("/create-admin-user")
	public ResponseEntity<ApiResponse> createAdminUser() {
		User adminUser = customerService.createAdminUser();
		return new ResponseEntity<ApiResponse>(
				new ApiResponse(HttpStatus.OK.value(), "Admin saved successfully.", adminUser, false), new HttpHeaders(),
				HttpStatus.OK);
	}
	
	@CrossOrigin
	@GetMapping
	public ResponseEntity<ApiResponse> getAllCustomer() {
		List<User> listOfCustomer = customerService.getAllCustomers();
		return new ResponseEntity<ApiResponse>(
				new ApiResponse(HttpStatus.OK.value(), "Get all customer", listOfCustomer, false), new HttpHeaders(),
				HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("user-by-id/{user_id}")
	public ResponseEntity<ApiResponse> getUserById(@PathVariable String user_id) {
		User user = customerService.getUserById(user_id);
		return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.OK.value(), "Get by user id", user, false),
				new HttpHeaders(), HttpStatus.OK);
	}

	@CrossOrigin
	@PutMapping("/{id}")
	public ResponseEntity<ApiResponse> updateUser(@PathVariable String id, @RequestBody CustomerDTO input) {
		if (input == null) {
			throw new BadRequestException("Json object can't be null", false);
		}
		User user = customerService.updateUser(id, input);
		return new ResponseEntity<ApiResponse>(
				new ApiResponse(HttpStatus.OK.value(), "Customer update successfully.", user, false), new HttpHeaders(),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/saveCusotmer")
	public ResponseEntity<ApiResponse> saveCusotmer(@RequestBody MyInputRequest input) {
		if (input.getJsonOfObject() == null) {
			throw new BadRequestException("Json object can't be null", false);
		}
		System.err.println(input.getJsonOfObject());
		User customer = customerService
				.saveCusotmer(VariableUtils.MAPPER.convertValue(input.getJsonOfObject(), CustomerDTO.class));
		return new ResponseEntity<ApiResponse>(
				new ApiResponse(HttpStatus.OK.value(), "Customer saved successfully.", null, false), new HttpHeaders(),
				HttpStatus.OK);
	}

	@CrossOrigin
	@PostMapping("/upload-customer")
	public ResponseEntity<ApiResponse> uploadCustomers(@RequestParam MultipartFile file) {
		List<String> errors = customerService.uploadCustomer(file);
		if (errors.size() == 0) {
			return new ResponseEntity<ApiResponse>(
					new ApiResponse(HttpStatus.OK.value(), "Customer Data uploaded successfully.", null, false),
					new HttpHeaders(), HttpStatus.OK);
		}
		return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.CONFLICT.value(), "", errors, true),
				new HttpHeaders(), HttpStatus.CONFLICT);

	}

	@CrossOrigin
	@PostMapping("/upload-customer-files")
	public ResponseEntity<ApiResponse> uploadCustomerFiles(@RequestParam String year,
			@RequestParam MultipartFile file) {
		String msg = customerService.uploadCustomerFiles(year, file);
		return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.OK.value(), msg, null, false),
				new HttpHeaders(), HttpStatus.OK);

	}

	public Resource loadFileAsResource(String customerId, String year) throws FileNotFoundException {
		File file = customerService.customerMergeDocument(customerId, year);
		InputStreamResource isr = new InputStreamResource(new FileInputStream(file));
		return isr;
	}

	@CrossOrigin
	@GetMapping(value = "/download-file1", produces = "application/pdf; charset=utf-8")
	public ResponseEntity<Resource> downloadFile1(@RequestParam String customerId, @RequestParam String year, HttpServletResponse response,
			HttpServletRequest request) {

		Resource resource = null;
		try {
			resource = this.loadFileAsResource(customerId, year);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            System.err.println("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
	}

	@CrossOrigin
	@PostMapping("/change-password")
	public ResponseEntity<ApiResponse> changePassword(@RequestBody ChangePasswordDTO input) {
		try {
			System.err.println(VariableUtils.MAPPER.writeValueAsString(input));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		String msg = customerService.changePassword(input);
		return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.OK.value(), msg, null, false),
				new HttpHeaders(), HttpStatus.OK);

	}

	@CrossOrigin
	@PostMapping("/change-admin-password")
	public ResponseEntity<ApiResponse> changeAdminPassword(@RequestBody ChangePasswordDTO input) {
		try {
			System.err.println(VariableUtils.MAPPER.writeValueAsString(input));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		String msg = customerService.changeAdminPassword(input);
		return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.OK.value(), msg, null, false),
				new HttpHeaders(), HttpStatus.OK);

	}

	@CrossOrigin
	@PostMapping("/uploadZipFile")
	public void uploadZipFile(@RequestParam MultipartFile file) {
//		List<String> errors = customerService.uploadCustomer(file);
//		if (errors.size() == 0) {
//			return new ResponseEntity<ApiResponse>(
//					new ApiResponse(HttpStatus.OK.value(), "Customer Data uploaded successfully.", null, false),
//					new HttpHeaders(), HttpStatus.OK);
//		}
//		return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.CONFLICT.value(), "", errors, true),
//				new HttpHeaders(), HttpStatus.CONFLICT);		

	}

	@CrossOrigin
	@GetMapping("/{user_id}/change-block-status/{status}")
	public ResponseEntity<ApiResponse> changeBlockStatus(@PathVariable("user_id") String userId,
			@PathVariable("status") boolean status) {
		String msg = customerService.changeUserBlockStaus(userId, status);
		return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.OK.value(), msg, null, false),
				new HttpHeaders(), HttpStatus.OK);
	}

	@CrossOrigin
	@GetMapping("appCalled")
	public List<Documents> appCalled() {
		return documentRepository.findAllByYearAndUser("2019", new User("4028ab6b6ba7a9b8016ba7be2e050006"));
	}
}

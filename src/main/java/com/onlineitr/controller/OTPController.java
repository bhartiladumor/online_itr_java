package com.onlineitr.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.onlineitr.exception.ApiResponse;
import com.onlineitr.exception.BadRequestException;
import com.onlineitr.service.CustomerService;
import com.onlineitr.utils.MethodUtils;

@RestController
@RequestMapping("/api/otp/")
public class OTPController {

	@Autowired
	private CustomerService customerService;

	@GetMapping("send")
	@CrossOrigin
	public ResponseEntity<ApiResponse> sendOTP(@RequestParam String mobile_no, @RequestParam String user_id) {

		if (MethodUtils.isObjectNullOrEmpty(mobile_no)) {
			throw new BadRequestException("Please enter mobile no", false);
		}

		Map<String, String> mapOfResponse = customerService.sendOTP(mobile_no, user_id);

		System.err.println(mapOfResponse);
		if (mapOfResponse.containsKey("Status")) {

			String value = mapOfResponse.get("Status");
			if ("Error".equalsIgnoreCase(value)) {
				return new ResponseEntity<ApiResponse>(
						new ApiResponse(HttpStatus.CONFLICT.value(), mapOfResponse.get("Details"), null, false),
						new HttpHeaders(), HttpStatus.CONFLICT);
			} else {
				return new ResponseEntity<ApiResponse>(
						new ApiResponse(HttpStatus.OK.value(), "OTP send to your mobile no. ", null, false),
						new HttpHeaders(), HttpStatus.OK);
			}

		}
		return null;
	}

	@GetMapping("verify")
	@CrossOrigin
	public ResponseEntity<ApiResponse> verifyOTP(@RequestParam String otp, @RequestParam String user_id) {
		if (MethodUtils.isObjectNullOrEmpty(otp) || MethodUtils.isObjectNullOrEmpty(user_id)) {
			throw new BadRequestException("Incomplete request", false);
		}

		return customerService.verifyOTP(otp, user_id);
	}

}

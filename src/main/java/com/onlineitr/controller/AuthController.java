package com.onlineitr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlineitr.exception.ApiResponse;
import com.onlineitr.model.User;
import com.onlineitr.payload.LoginRequest;
import com.onlineitr.service.CustomerService;

@RestController
@RequestMapping("/api/auth/")
public class AuthController {

	@Autowired
	CustomerService customerService;

	@CrossOrigin
	@PostMapping("login")
	public ResponseEntity<ApiResponse> login(@RequestBody LoginRequest request) {

		User customer = customerService.login(request);
		if (customer == null) {
			return new ResponseEntity<ApiResponse>(
					new ApiResponse(HttpStatus.BAD_REQUEST.value(), "Invalid username or password or may be blocked by admin", customer, false),
					new HttpHeaders(), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ApiResponse>(
				new ApiResponse(HttpStatus.OK.value(), "Welcome ! " + customer.getUsername(), customer, false),
				new HttpHeaders(), HttpStatus.OK);
	}
}

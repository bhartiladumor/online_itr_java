package com.onlineitr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.onlineitr.exception.ApiResponse;
import com.onlineitr.exception.BadRequestException;
import com.onlineitr.model.UserRole;
import com.onlineitr.payload.MyInputRequest;
import com.onlineitr.service.UserRoleService;
import com.onlineitr.utils.VariableUtils;

@Controller
@RequestMapping("api/userRole")
public class UserRoleController {

	@Autowired
	private UserRoleService userRoleService;

	@CrossOrigin
	@PostMapping("/")
	public ResponseEntity<ApiResponse> saveUserRole(@RequestBody MyInputRequest input) {
		if(input.getJsonOfObject() == null) {
			throw new BadRequestException("Json object can't be null" , false);
		}
		UserRole userRole = userRoleService
				.save(VariableUtils.MAPPER.convertValue(input.getJsonOfObject(), UserRole.class));
		return new ResponseEntity<ApiResponse>(
				new ApiResponse(HttpStatus.OK.value(), "Customer saved successfully.", userRole, false),
				new HttpHeaders(), HttpStatus.OK);
	}
}

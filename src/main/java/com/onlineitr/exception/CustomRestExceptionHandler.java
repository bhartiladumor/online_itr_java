package com.onlineitr.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<Object> handleBadRequestException(HttpServletRequest request, BadRequestException ex) {
		ApiResponse apiError = new ApiResponse(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), ex.getMsg(),
				null,ex.getIsReponseOnPage());
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ConflictException.class)
	public ResponseEntity<Object> handleConflictException(HttpServletRequest request, ConflictException ex) {
		ApiResponse apiError = new ApiResponse(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), ex.getMsg(),
				null , ex.getIsReponseOnPage());
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<Object> handleEntityNotFoundException(HttpServletRequest request,
			EntityNotFoundException ex) {
		System.err.println("1111111111111111111111111");
		ApiResponse apiError = new ApiResponse(HttpStatus.NOT_FOUND.value(), ex.getLocalizedMessage(), ex.getMsg(),
				null , ex.getIsReponseOnPage());
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		String error = ex.getParameterName() + " parameter is missing";
		ApiResponse apiError = new ApiResponse(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), error , false);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ MethodArgumentTypeMismatchException.class })
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
			WebRequest request) {
		String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
		ApiResponse apiError = new ApiResponse(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), error , false);
		return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST);
	}

//	@ExceptionHandler({ Exception.class })
//	public ResponseEntity<Object> handleAll(Exception ex, WebRequest request) {
//		System.err.println(ex.getMessage());
//		ApiResponse apiError = new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getLocalizedMessage(),
//				"error occurred");
//		return new ResponseEntity<Object>(apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
//	}

}

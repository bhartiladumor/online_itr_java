package com.onlineitr.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String msg;
	private Boolean isReponseOnPage;

	public BadRequestException(String msg, Boolean isReponseOnPage) {
		this.msg = msg;
		this.isReponseOnPage = isReponseOnPage;

	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Boolean getIsReponseOnPage() {
		return isReponseOnPage;
	}

	public void setIsReponseOnPage(Boolean isReponseOnPage) {
		this.isReponseOnPage = isReponseOnPage;
	}

}

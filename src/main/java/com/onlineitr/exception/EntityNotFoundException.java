package com.onlineitr.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

	private String entityName;
	private Boolean isReponseOnPage;

	public EntityNotFoundException(String entityName , Boolean isReponseOnPage) {
		this.entityName = entityName;
		this.isReponseOnPage = isReponseOnPage;
	}

	public String getMsg() {
		return this.entityName + " not found.";
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public Boolean getIsReponseOnPage() {
		return isReponseOnPage;
	}

	public void setIsReponseOnPage(Boolean isReponseOnPage) {
		this.isReponseOnPage = isReponseOnPage;
	}
	

}

package com.onlineitr.exception;

import org.springframework.core.io.InputStreamResource;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(value = Include.NON_NULL)
public class ApiResponse {

	private int status;
    private String message;
    private Object data;
    private String error;
    private Boolean isResponseOnPage;
    private InputStreamResource isr;    
  
    public ApiResponse(int status, String message, String errors , Object data ,  Boolean isResponseOnPage) {
        super();
        this.status = status;
        this.message = message;
        this.error = errors;
        this.data = data;
        this.isResponseOnPage = isResponseOnPage;
    }
    
    public ApiResponse(int status, String message, Object data , Boolean isResponseOnPage) {
    	super();
        this.status = status;
        this.message = message;
        this.data = data;
        this.isResponseOnPage = isResponseOnPage;
    }
    
    public ApiResponse(int status, String message, InputStreamResource isr, Boolean isResponseOnPage) {
    	super();
        this.status = status;
        this.message = message;
        this.isResponseOnPage = isResponseOnPage;
        this.isr = isr;
    }

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Boolean getIsResponseOnPage() {
		return isResponseOnPage;
	}

	public void setIsResponseOnPage(Boolean isResponseOnPage) {
		this.isResponseOnPage = isResponseOnPage;
	}

	public InputStreamResource getIsr() {
		return isr;
	}

	public void setIsr(InputStreamResource isr) {
		this.isr = isr;
	}
}

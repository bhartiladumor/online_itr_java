package com.onlineitr.scheduler;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class StorageMonitor {
	
	private static final Logger logger = LogManager.getLogger(StorageMonitor.class);

	@Scheduled(cron = "0 0 0 * * ?")
	public void storageMonitorJob() throws IOException {
		logger.info("Scheduler start.");
		File dir = new File(System.getProperty("java.io.tmpdir") + File.separator + "itr");
		System.err.println(dir.getAbsolutePath());
		if (dir.isDirectory() == false) {
			logger.info("Not a directory. Do nothing");
			return;
		}
		logger.info("Check if size of directory is exceed the size 1GB");
		if ((FileUtils.sizeOfDirectory(dir) / FileUtils.ONE_KB) > 1) {
			File[] listFiles = dir.listFiles();
			logger.info(
					"Check all folder inside directory one by one and if folder modified before 1 hour then delete it ");
			for (File file : listFiles) {
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MINUTE, -1);
				Date oneHourBack = cal.getTime();
				if (file.lastModified() < oneHourBack.getTime()) {
					if (file.isDirectory()) {
						FileUtils.deleteDirectory(file);
					} else {
						file.delete();
					}
				}
			}
		}

		logger.info("Scheduler end.");
	}
}

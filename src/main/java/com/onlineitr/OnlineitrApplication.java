
package com.onlineitr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication(scanBasePackages = {"com.onlineitr"})
@EnableJpaRepositories(basePackages = "com.onlineitr.repository")
@ComponentScan(basePackages = "com.onlineitr")
@EnableScheduling
public class OnlineitrApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OnlineitrApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(OnlineitrApplication.class, args);
	}

	@GetMapping("/appCalled")
	public String appCalled() {
		return "app called";
	}
}

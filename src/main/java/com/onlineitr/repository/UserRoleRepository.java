package com.onlineitr.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlineitr.model.UserRole;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, String> {
	
	public Optional<UserRole> findByName(String name);

}

package com.onlineitr.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.onlineitr.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

	Optional<User> findByPanCardNo(String panCardNo);

	Optional<User> findByUsername(String username);

	Optional<User> findByUsernameAndPasswordAndIsUserActiveTrue(String username, String password);

	Optional<User> findById(String id);

	@Query("SELECT new com.onlineitr.model.User(aliasUser.id , aliasUser.username ,aliasUser.password , aliasUser.panCardNo , aliasUser.activeStatus , aliasUser.isUserActive) FROM User aliasUser LEFT JOIN UserRole aliasUserRole ON aliasUser.userRole.id = aliasUserRole.id where aliasUserRole.name = ?1")
	List<User> getAllUserWithUserRole(String userRole);
	
	@Query("SELECT aliasUser from User aliasUser INNER JOIN Documents aliasDocuments ON aliasDocuments.user.id = aliasUser.id where aliasDocuments.year='2019' and aliasUser.id.username = ?1")
	List<User> findUserWithDocument(String username);
	
	Optional<User> findByUsernameAndUserRoleName(String username , String name);
}

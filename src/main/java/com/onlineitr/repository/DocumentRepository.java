package com.onlineitr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlineitr.model.Documents;
import com.onlineitr.model.User;

@Repository
public interface DocumentRepository extends JpaRepository<Documents, String> {

	List<Documents> findAllByYearAndUser(String year , User user);
}

package com.onlineitr.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipTest {
	
	public static void main(String[] args) throws IOException {
		String fileZip = "D:\\5cf1cd10-24ca-46ed-91de-da2edc68f5c5.zip";
		File destDir = new File("D:\\");
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            System.err.println(newFile.getAbsolutePath());
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
	}

	public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
		System.out.println(zipEntry.getName());
        File destFile = new File(destinationDir, zipEntry.getName());
         
        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();
        System.out.println(destDirPath);
        System.out.println(destFilePath);
        System.out.println(destDirPath + File.separator);
         
//        if (!destFilePath.startsWith(destDirPath + File.separator)) {
//            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
//        }
         
        return destFile;
    }
}

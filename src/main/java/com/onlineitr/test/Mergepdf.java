package com.onlineitr.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.onlineitr.pdf.FileConversions;

public class Mergepdf {
	public static void main(String[] args) throws DocumentException, IOException {
//		List<InputStream> list = new ArrayList<InputStream>();
//		list.add(new FileInputStream(new File("D:\\say_solution\\documents\\1.pdf")));
//		list.add(new FileInputStream(new File("D:\\say_solution\\documents\\2.pdf")));

//		OutputStream outputStream = new FileOutputStream(new File("D:\\say_solution\\documents\\result.pdf"));
//		Document document = new Document();
//		PdfWriter writer = PdfWriter.getInstance(document, outputStream);
//		document.open();
//		PdfContentByte cb = writer.getDirectContent();
//
//		for (InputStream in : list) {
//			PdfReader reader = new PdfReader(in);
//			reader = unlockPdf(reader);
//			for (int i = 1; i <= reader.getNumberOfPages(); i++) {
//				document.newPage();
//				// import the page from source pdf
//				PdfImportedPage page = writer.getImportedPage(reader, i);
//				// add the page to the destination pdf
//				cb.addTemplate(page, 0, 0);
//			}
//		}
//
//		outputStream.flush();
//		document.close();
//		outputStream.close();
//
//		File file1 = new File("D:\\say_solution\\documents\\1.pdf");
//		PDDocument doc1 = PDDocument.load(file1);
//
//		File file2 = new File("D:\\say_solution\\documents\\2.pdf");
//		PDDocument doc2 = PDDocument.load(file2);
//
//		// Instantiating PDFMergerUtility class
//		PDFMergerUtility PDFmerger = new PDFMergerUtility();
//
//		// Setting the destination file
//		PDFmerger.setDestinationFileName("D:\\say_solution\\documents\\result.pdf");
//
//		// adding the source filess
//		PDFmerger.addSource(file1);
//		PDFmerger.addSource(file2);
//
//		// Merging the two documents
//		PDFmerger.mergeDocuments();
//		System.out.println("Documents merged");
//
//		// Closing the documents
//		doc1.close();
//		doc2.close();

		List<File> list = new ArrayList<File>();
		list.add(new File("D:\\say_solution\\documents\\1.pdf"));
		list.add(new File("D:\\say_solution\\documents\\2.pdf"));
//		FileConversions.mergePDFDocuments(list);

		System.err.println("qwwwwwwwwwwwwwwwwwwwwwww");
	}

	public static PdfReader unlockPdf(PdfReader reader) {
		if (reader == null) {
			return reader;
		}
		try {
			Field f = reader.getClass().getDeclaredField("encrypted");
			f.setAccessible(true);
			f.set(reader, false);
		} catch (Exception e) { // ignore }
			return reader;
		}

		return reader;
	}

}

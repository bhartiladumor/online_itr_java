package com.onlineitr.model;

import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.onlineitr.utils.MethodUtils;
import com.onlineitr.utils.VariableUtils;

@Entity
@Table(name = VariableUtils.TBL_USER_ROLE)
@JsonInclude(value = Include.NON_EMPTY)
public class UserRole {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "userRoleId", unique = true)
	private String id;

	
	private String name;

	@OneToMany(mappedBy = "userRole")
	private Set<User> setOfUser;
	
	@PreRemove
	protected void onRemove() {
		Set<User> users = this.getSetOfUser();
		if (!MethodUtils.isSetNullOrEmpty(users)) {
			users.forEach(user -> {
				user.setUserRole(null);
			});
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<User> getSetOfUser() {
		return setOfUser;
	}

	public void setSetOfUser(Set<User> setOfUser) {
		this.setOfUser = setOfUser;
	}
}

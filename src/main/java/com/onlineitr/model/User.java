package com.onlineitr.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.onlineitr.utils.MethodUtils;
import com.onlineitr.utils.VariableUtils;

@Entity
@Table(name = VariableUtils.TBL_USER)
@JsonInclude(value = Include.NON_EMPTY)
public class User {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "userId", unique = true)
	private String id;

	public User() {

	}

	public User(String id) {
		this.id = id;
	}
	
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public User(String id , String username, String password, String panCardNo , boolean activeStatus , boolean isUserActive) {
		this.id = id;
		this.username = username;
		this.password = null;
		this.panCardNo = panCardNo;
		this.activeStatus = activeStatus;
		this.isUserActive = isUserActive;
	}
	
	public User(String username, String password, String panCardNo) {
		this.username = username;
		this.password = password;
		this.panCardNo = panCardNo;
	}

	private String username;
	private String password;
	private String panCardNo;
	private String firstName;
	private String lastName;
	private String email;
	private String aadharCardNo;
	private String address;
	
	
	@Type(type="yes_no")
	private boolean activeStatus;
	
	@Type(type="yes_no")
	private boolean isUserActive;

	@OneToMany(mappedBy = "user")
	private Set<Documents> setOfDocuments;

	@ManyToOne
	@JsonIgnore
	private UserRole userRole;

	@Transient
	private Map<String, Object> mapOfOthers;
	
	private String tempOtp;
	
	@ElementCollection
	private Set<String> userMobileNo;

	@PrePersist
	protected void onCreate() {
		if (!MethodUtils.isObjectNullOrEmpty(this.username)) {
			this.panCardNo = this.username;
			this.password = this.username;
		}
	}

	@PreRemove
	protected void onRemove() {
		Set<Documents> docs = this.getSetOfDocuments();
		if (!MethodUtils.isSetNullOrEmpty(docs)) {
			docs.forEach(doc -> doc.setUser(null));
		}

		UserRole role = this.getUserRole();
		if (role != null) {
			role.getSetOfUser().remove(this);
		}

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPanCardNo() {
		return panCardNo;
	}

	public void setPanCardNo(String panCardNo) {
		this.panCardNo = panCardNo;
	}

	public Set<Documents> getSetOfDocuments() {
		return setOfDocuments;
	}

	public void setSetOfDocuments(Set<Documents> setOfDocuments) {
		this.setOfDocuments = setOfDocuments;
	}

	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public Map<String, Object> getMapOfOthers() {
		mapOfOthers = new HashMap<String, Object>();
		if (this.userRole != null) {
			mapOfOthers.put("userRoleName", this.userRole.getName());
		}
		return mapOfOthers;
	}

	public void setMapOfOthers(Map<String, Object> mapOfOthers) {
		this.mapOfOthers = mapOfOthers;
	}

	public String getTempOtp() {
		return tempOtp;
	}

	public void setTempOtp(String tempOtp) {
		this.tempOtp = tempOtp;
	}

	public boolean isActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public boolean isUserActive() {
		return isUserActive;
	}

	public void setUserActive(boolean isUserActive) {
		this.isUserActive = isUserActive;
	}

	public Set<String> getUserMobileNo() {
		return userMobileNo;
	}

	public void setUserMobileNo(Set<String> userMobileNo) {
		this.userMobileNo = userMobileNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAadharCardNo() {
		return aadharCardNo;
	}

	public void setAadharCardNo(String aadharCardNo) {
		this.aadharCardNo = aadharCardNo;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}

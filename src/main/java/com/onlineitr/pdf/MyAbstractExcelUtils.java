package com.onlineitr.pdf;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFColor;

public class MyAbstractExcelUtils {

	/* package */ static final String EMPTY = "";
	private static final short EXCEL_COLUMN_WIDTH_FACTOR = 256;
	private static final int UNIT_OFFSET_LENGTH = 7;

	/**
	 * @param alignment The horizontal alignment. See {@link HorizontalAlignment}.
	 * @return a friendly string representation of the alignment code
	 * @deprecated POI 3.15 beta 3. Use {@link #getAlign(HorizontalAlignment)}
	 *             instead.
	 */
	public static String getAlign(short alignment) {
		return getAlign(HorizontalAlignment.forInt(alignment));
	}

	public static String getAlign(HorizontalAlignment alignment) {
		switch (alignment) {
		case CENTER:
			return "center";
		case CENTER_SELECTION:
			return "center";
		case FILL:
			// XXX: shall we support fill?
			return "";
		case GENERAL:
			return "";
		case JUSTIFY:
			return "justify";
		case LEFT:
			return "left";
		case RIGHT:
			return "right";
		default:
			return "";
		}
	}

	public static String getBorderStyle(BorderStyle xlsBorder) {
		final String borderStyle;
		switch (xlsBorder) {
		case NONE:
			borderStyle = "none";
			break;
		case DASH_DOT:
		case DASH_DOT_DOT:
		case DOTTED:
		case HAIR:
		case MEDIUM_DASH_DOT:
		case MEDIUM_DASH_DOT_DOT:
		case SLANTED_DASH_DOT:
			borderStyle = "dotted";
			break;
		case DASHED:
		case MEDIUM_DASHED:
			borderStyle = "dashed";
			break;
		case DOUBLE:
			borderStyle = "double";
			break;
		default:
			borderStyle = "solid";
			break;
		}
		return borderStyle;
	}

	public static String getBorderWidth(BorderStyle xlsBorder) {
		final String borderWidth;
		switch (xlsBorder) {
		case MEDIUM_DASH_DOT:
		case MEDIUM_DASH_DOT_DOT:
		case MEDIUM_DASHED:
			borderWidth = "2pt";
			break;
		case THICK:
			borderWidth = "thick";
			break;
		default:
			borderWidth = "thin";
			break;
		}
		return borderWidth;
	}

	public static String getColor(XSSFColor color) {
		StringBuilder stringBuilder = new StringBuilder(7);
		stringBuilder.append('#');
		for (short s : color.getRGB()) {
			if (s < 10)
				stringBuilder.append('0');

			stringBuilder.append(Integer.toHexString(s));
		}
		String result = stringBuilder.toString();

		if (result.equals("#ffffff"))
			return "white";

		if (result.equals("#c0c0c0"))
			return "silver";

		if (result.equals("#808080"))
			return "gray";

		if (result.equals("#000000"))
			return "black";

		return result;
	}

}

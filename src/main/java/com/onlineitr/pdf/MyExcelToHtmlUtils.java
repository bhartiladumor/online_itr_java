package com.onlineitr.pdf;

import java.util.Arrays;

import org.apache.poi.hssf.converter.AbstractExcelUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFColor;

public class MyExcelToHtmlUtils extends AbstractExcelUtils {
	
	static final String EMPTY = "";
	
	static boolean isEmpty(String str) {
		return str == null || str.length() == 0;
	}

	public static void appendAlign(StringBuilder style, short alignment) {
		String cssAlign = getAlign(alignment);
		if (isEmpty(cssAlign))
			return;

		style.append("text-align:");
		style.append(cssAlign);
		style.append(";");
	}
	
	public static String getColor( XSSFColor color )
    {
        StringBuilder stringBuilder = new StringBuilder( 7 );
        stringBuilder.append( '#' );
        for ( short s : color.getRGB() )
        {
            if ( s < 10 )
                stringBuilder.append( '0' );

            stringBuilder.append( Integer.toHexString( s ) );
        }
        String result = stringBuilder.toString();

        if ( result.equals( "#ffffff" ) )
            return "white";

        if ( result.equals( "#c0c0c0" ) )
            return "silver";

        if ( result.equals( "#808080" ) )
            return "gray";

        if ( result.equals( "#000000" ) )
            return "black";

        return result;
    }

	/**
	 * Creates a map (i.e. two-dimensional array) filled with ranges. Allow fast
	 * retrieving {@link CellRangeAddress} of any cell, if cell is contained in
	 * range.
	 * 
	 * @see #getMergedRange(CellRangeAddress[][], int, int)
	 */
	public static CellRangeAddress[][] buildMergedRangesMap(HSSFSheet sheet) {
		CellRangeAddress[][] mergedRanges = new CellRangeAddress[1][];
		for (final CellRangeAddress cellRangeAddress : sheet.getMergedRegions()) {
			final int requiredHeight = cellRangeAddress.getLastRow() + 1;
			if (mergedRanges.length < requiredHeight) {
				CellRangeAddress[][] newArray = new CellRangeAddress[requiredHeight][];
				System.arraycopy(mergedRanges, 0, newArray, 0, mergedRanges.length);
				mergedRanges = newArray;
			}

			for (int r = cellRangeAddress.getFirstRow(); r <= cellRangeAddress.getLastRow(); r++) {
				final int requiredWidth = cellRangeAddress.getLastColumn() + 1;

				CellRangeAddress[] rowMerged = mergedRanges[r];
				if (rowMerged == null) {
					rowMerged = new CellRangeAddress[requiredWidth];
					mergedRanges[r] = rowMerged;
				} else {
					final int rowMergedLength = rowMerged.length;
					if (rowMergedLength < requiredWidth) {
						final CellRangeAddress[] newRow = new CellRangeAddress[requiredWidth];
						System.arraycopy(rowMerged, 0, newRow, 0, rowMergedLength);

						mergedRanges[r] = newRow;
						rowMerged = newRow;
					}
				}

				Arrays.fill(rowMerged, cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn() + 1,
						cellRangeAddress);
			}
		}
		return mergedRanges;
	}

}

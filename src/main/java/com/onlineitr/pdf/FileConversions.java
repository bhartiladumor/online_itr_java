package com.onlineitr.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;

public class FileConversions {

	public static File convertRtfFileToPDF(String uploadedFileName, String inputFilePath, String outputDirPath) {

		try {
			System.out.println(System.getProperty("java.io.tmpdir"));
			File inputFile = new File(System.getProperty("java.io.tmpdir") + File.separator + "itr");
			if (!inputFile.exists()) {
				boolean dir = inputFile.mkdir();
			}

			File outputFile = new File(outputDirPath + File.separator
					+ uploadedFileName.substring(0, (uploadedFileName.lastIndexOf(".") + 1)) + "pdf");
			IConverter converter = LocalConverter.builder().baseFolder(inputFile)
					.workerPool(20, 25, 2, TimeUnit.SECONDS).processTimeout(5, TimeUnit.SECONDS).build();

			Future<Boolean> conversion = converter.convert(new File(inputFilePath))
					.as(com.documents4j.api.DocumentType.RTF).to(outputFile).as(com.documents4j.api.DocumentType.PDF)
					.prioritizeWith(1000).schedule();
			conversion.get();
			return outputFile;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static File convertExcelToPDF(String uploadedFileName, String inputFilePath, String outputDirPath) {

		try {

			File inputFile = new File(inputFilePath);
			File outputFile = new File(outputDirPath + File.separator
					+ uploadedFileName.substring(0, (uploadedFileName.lastIndexOf(".") + 1)) + "pdf");

			Workbook workbook = WorkbookFactory.create(new FileInputStream(inputFile));
			if (workbook instanceof XSSFWorkbook) {
				Document doc = MyExcelToHTMLConverters.process(inputFile);
				writePdf(doc, outputFile);
			} else {
				Document doc = ExcelToHTMLConverters.process(inputFile);
				writePdf(doc, outputFile);
			}
			return outputFile;
		} catch (EncryptedDocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void writePdf(Document doc, File outputFile) {
		try {
			FileOutputStream out = new FileOutputStream(outputFile);
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(doc, null);
			renderer.layout();
			renderer.createPDF(out);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static File mergePDFDocuments(List<File> files, String fileName) {
		try {

			File outputFile = new File(fileName);
			// Instantiating PDFMergerUtility class
			PDFMergerUtility pdfMerger = new PDFMergerUtility();

			// Setting the destination file
			pdfMerger.setDestinationFileName(outputFile.getAbsolutePath());

			System.out.println("size :: " + files.size());
			files.forEach(file -> {
				try {
					System.err.println(file.getName());
					PDDocument doc1 = PDDocument.load(file);
					pdfMerger.addSource(file);
					pdfMerger.mergeDocuments();
					doc1.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			});
//			pdfMerger.mergeDocuments();
			return outputFile;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

}

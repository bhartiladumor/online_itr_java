package com.onlineitr.pdf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.hssf.converter.AbstractExcelConverter;
import org.apache.poi.hssf.converter.ExcelToHtmlUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hwpf.converter.HtmlDocumentFacade;
import org.apache.poi.ss.formula.eval.ErrorEval;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.XMLHelper;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class MyExcelToHTMLConverters extends AbstractExcelConverter {

	private String cssClassContainerCell = null;

	private String cssClassContainerDiv = null;

	private String cssClassPrefixCell = "c";

	private String cssClassPrefixDiv = "d";

	private String cssClassPrefixRow = "r";

	private String cssClassPrefixTable = "t";

	private Map<Short, String> excelStyleToClass = new LinkedHashMap<Short, String>();

	private final HtmlDocumentFacade htmlDocumentFacade;

	private boolean useDivsToSpan = false;

	public MyExcelToHTMLConverters(Document doc) {
		htmlDocumentFacade = new HtmlDocumentFacade(doc);
	}

	public MyExcelToHTMLConverters(HtmlDocumentFacade htmlDocumentFacade) {
		this.htmlDocumentFacade = htmlDocumentFacade;
	}

	@Override
	protected Document getDocument() {
		return htmlDocumentFacade.getDocument();
	}

	public static Document process(File xlsFile) throws IOException, ParserConfigurationException {

		FileInputStream inputStream = new FileInputStream(xlsFile);

		final XSSFWorkbook workbook = new XSSFWorkbook(inputStream);

		MyExcelToHTMLConverters xssfPdfConverter = new MyExcelToHTMLConverters(
				XMLHelper.getDocumentBuilderFactory().newDocumentBuilder().newDocument());
		xssfPdfConverter.processWorkbook(workbook);
		Document doc = xssfPdfConverter.getDocument();
		workbook.close();
		return doc;
	}

	public void processWorkbook(XSSFWorkbook workbook) {

		if (isUseDivsToSpan()) {
			// prepare CSS classes for later usage
			this.cssClassContainerCell = htmlDocumentFacade.getOrCreateCssClass(cssClassPrefixCell,
					"padding:0;margin:0;align:left;vertical-align:top;");
			this.cssClassContainerDiv = htmlDocumentFacade.getOrCreateCssClass(cssClassPrefixDiv, "position:relative;");
		}
//        workbook.getNumberOfSheets()
		for (int s = 0; s < 2; s++) {
			XSSFSheet sheet = workbook.getSheetAt(s);
			processSheet(sheet);
		}

		htmlDocumentFacade.updateStylesheet();

	}

	protected void processSheet(XSSFSheet sheet) {
//      processSheetHeader( htmlDocumentFacade.getBody(), sheet );

		final int physicalNumberOfRows = sheet.getPhysicalNumberOfRows();
		if (physicalNumberOfRows <= 0)
			return;

		Element table = htmlDocumentFacade.createTable();
		htmlDocumentFacade.addStyleClass(table, cssClassPrefixTable, "border-collapse:collapse;border-spacing:0;");

		Element tableBody = htmlDocumentFacade.createTableBody();

		final CellRangeAddress[][] mergedRanges = MyExcelToHTMLConverters.buildMergedRangesMap(sheet);

		final List<Element> emptyRowElements = new ArrayList<Element>(physicalNumberOfRows);
		int maxSheetColumns = 1;

		for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
			XSSFRow row = sheet.getRow(r);

			if (row == null)
				continue;

			if (!isOutputHiddenRows() && row.getZeroHeight())
				continue;

			Element tableRowElement = htmlDocumentFacade.createTableRow();
			htmlDocumentFacade.addStyleClass(tableRowElement, cssClassPrefixRow,
					"height:" + (row.getHeight() / 20f) + "pt;");

			int maxRowColumnNumber = processRow(mergedRanges, row, tableRowElement);

			if (maxRowColumnNumber == 0) {
				emptyRowElements.add(tableRowElement);
			} else {
				if (!emptyRowElements.isEmpty()) {
					for (Element emptyRowElement : emptyRowElements) {
						tableBody.appendChild(emptyRowElement);
					}
					emptyRowElements.clear();
				}

				tableBody.appendChild(tableRowElement);
			}
			maxSheetColumns = Math.max(maxSheetColumns, maxRowColumnNumber);
		}

		processColumnWidths(sheet, maxSheetColumns, table);

		if (isOutputColumnHeaders()) {
//			processColumnHeaders(sheet, maxSheetColumns, table);
		}

		table.appendChild(tableBody);

		htmlDocumentFacade.getBody().appendChild(table);
	}

	/**
	 * Creates COLGROUP element with width specified for all columns. (Except first
	 * if <tt>{@link #isOutputRowNumbers()}==true</tt>)
	 */
	protected void processColumnWidths(XSSFSheet sheet, int maxSheetColumns, Element table) {
		// draw COLS after we know max column number
		Element columnGroup = htmlDocumentFacade.createTableColumnGroup();
		if (isOutputRowNumbers()) {
			columnGroup.appendChild(htmlDocumentFacade.createTableColumn());
		}
		for (int c = 0; c < maxSheetColumns; c++) {
			if (!isOutputHiddenColumns() && sheet.isColumnHidden(c))
				continue;

			Element col = htmlDocumentFacade.createTableColumn();
			col.setAttribute("width", String.valueOf(MyAbstractExcelConverter.getColumnWidth(sheet, c)));
			columnGroup.appendChild(col);
		}
		table.appendChild(columnGroup);
	}

	void buildStyle_font(XSSFWorkbook workbook, StringBuilder style, XSSFFont font) {
		switch (font.getBoldweight()) {
		case HSSFFont.BOLDWEIGHT_BOLD:
			style.append("font-weight:bold;");
			break;
		case HSSFFont.BOLDWEIGHT_NORMAL:
			// by default, not not increase HTML size
			// style.append( "font-weight: normal; " );
			break;
		}
		java.awt.Color col = new java.awt.Color(font.getColor());
		final XSSFColor fontColor = new XSSFColor(col);

//		final XSSFColor fontColor = workbook.getCustomPalette().getColor(font.getColor());
		if (fontColor != null)
			style.append("color: " + MyAbstractExcelUtils.getColor(fontColor) + "; ");

		if (font.getFontHeightInPoints() != 0)
			style.append("font-size:" + font.getFontHeightInPoints() + "pt;");

		if (font.getItalic()) {
			style.append("font-style:italic;");
		}
	}

	public String getCssClassPrefixCell() {
		return cssClassPrefixCell;
	}

	public String getCssClassPrefixDiv() {
		return cssClassPrefixDiv;
	}

	public String getCssClassPrefixRow() {
		return cssClassPrefixRow;
	}

	public String getCssClassPrefixTable() {
		return cssClassPrefixTable;
	}

	public boolean isUseDivsToSpan() {
		return useDivsToSpan;
	}

	/**
	 * Creates a map (i.e. two-dimensional array) filled with ranges. Allow fast
	 * retrieving {@link CellRangeAddress} of any cell, if cell is contained in
	 * range.
	 * 
	 * @see #getMergedRange(CellRangeAddress[][], int, int)
	 */
	public static CellRangeAddress[][] buildMergedRangesMap(XSSFSheet sheet) {
		CellRangeAddress[][] mergedRanges = new CellRangeAddress[1][];
		for (final CellRangeAddress cellRangeAddress : sheet.getMergedRegions()) {
			final int requiredHeight = cellRangeAddress.getLastRow() + 1;
			if (mergedRanges.length < requiredHeight) {
				CellRangeAddress[][] newArray = new CellRangeAddress[requiredHeight][];
				System.arraycopy(mergedRanges, 0, newArray, 0, mergedRanges.length);
				mergedRanges = newArray;
			}

			for (int r = cellRangeAddress.getFirstRow(); r <= cellRangeAddress.getLastRow(); r++) {
				final int requiredWidth = cellRangeAddress.getLastColumn() + 1;

				CellRangeAddress[] rowMerged = mergedRanges[r];
				if (rowMerged == null) {
					rowMerged = new CellRangeAddress[requiredWidth];
					mergedRanges[r] = rowMerged;
				} else {
					final int rowMergedLength = rowMerged.length;
					if (rowMergedLength < requiredWidth) {
						final CellRangeAddress[] newRow = new CellRangeAddress[requiredWidth];
						System.arraycopy(rowMerged, 0, newRow, 0, rowMergedLength);

						mergedRanges[r] = newRow;
						rowMerged = newRow;
					}
				}

				Arrays.fill(rowMerged, cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn() + 1,
						cellRangeAddress);
			}
		}
		return mergedRanges;
	}

	/**
	 * @return maximum 1-base index of column that were rendered, zero if none
	 */
	protected int processRow(CellRangeAddress[][] mergedRanges, XSSFRow row, Element tableRowElement) {
		final XSSFSheet sheet = row.getSheet();
		final short maxColIx = row.getLastCellNum();
		if (maxColIx <= 0)
			return 0;

		final List<Element> emptyCells = new ArrayList<Element>(maxColIx);

		if (isOutputRowNumbers()) {
//			Element tableRowNumberCellElement = htmlDocumentFacade.createTableHeaderCell();
//			processRowNumber(row, tableRowNumberCellElement);
//			emptyCells.add(tableRowNumberCellElement);
		}

		int maxRenderedColumn = 0;
		for (int colIx = 0; colIx < maxColIx; colIx++) {
			if (!isOutputHiddenColumns() && sheet.isColumnHidden(colIx))
				continue;

			CellRangeAddress range = ExcelToHtmlUtils.getMergedRange(mergedRanges, row.getRowNum(), colIx);

			if (range != null && (range.getFirstColumn() != colIx || range.getFirstRow() != row.getRowNum()))
				continue;

			XSSFCell cell = row.getCell(colIx);

			int divWidthPx = 0;
			if (isUseDivsToSpan()) {
				divWidthPx = MyAbstractExcelConverter.getColumnWidth(sheet, colIx);

				boolean hasBreaks = false;
				for (int nextColumnIndex = colIx + 1; nextColumnIndex < maxColIx; nextColumnIndex++) {
					if (!isOutputHiddenColumns() && sheet.isColumnHidden(nextColumnIndex))
						continue;

					if (row.getCell(nextColumnIndex) != null && !isTextEmpty(row.getCell(nextColumnIndex))) {
						hasBreaks = true;
						break;
					}

					divWidthPx += MyAbstractExcelConverter.getColumnWidth(sheet, nextColumnIndex);
				}

				if (!hasBreaks)
					divWidthPx = Integer.MAX_VALUE;
			}

			Element tableCellElement = htmlDocumentFacade.createTableCell();

			if (range != null) {
				if (range.getFirstColumn() != range.getLastColumn())
					tableCellElement.setAttribute("colspan",
							String.valueOf(range.getLastColumn() - range.getFirstColumn() + 1));
				if (range.getFirstRow() != range.getLastRow())
					tableCellElement.setAttribute("rowspan",
							String.valueOf(range.getLastRow() - range.getFirstRow() + 1));
			}

			boolean emptyCell;
			if (cell != null) {
				emptyCell = processCell(cell, tableCellElement, MyAbstractExcelConverter.getColumnWidth(sheet, colIx),
						divWidthPx, row.getHeight() / 20f);
			} else {
				emptyCell = true;
			}

			if (emptyCell) {
				emptyCells.add(tableCellElement);
			} else {
				for (Element emptyCellElement : emptyCells) {
					tableRowElement.appendChild(emptyCellElement);
				}
				emptyCells.clear();

				tableRowElement.appendChild(tableCellElement);
				maxRenderedColumn = colIx;
			}
		}

		return maxRenderedColumn + 1;
	}
	
	protected boolean processCell(XSSFCell cell, Element tableCellElement, int normalWidthPx, int maxSpannedWidthPx,
			float normalHeightPt) {
		final XSSFCellStyle cellStyle = cell.getCellStyle();

		String value;
		switch (cell.getCellTypeEnum()) {
		case STRING:
			// XXX: enrich
			value = cell.getRichStringCellValue().getString();
			break;
		case FORMULA:
			switch (cell.getCachedFormulaResultTypeEnum()) {
			case STRING:
				XSSFRichTextString str = cell.getRichStringCellValue();
				if (str != null && str.length() > 0) {
					value = (str.toString());
				} else {
					value = MyAbstractExcelConverter.EMPTY;
				}
				break;
			case NUMERIC:
				double nValue = cell.getNumericCellValue();
				short df = cellStyle.getDataFormat();
				String dfs = cellStyle.getDataFormatString();
				value = _formatter.formatRawCellContents(nValue, df, dfs);
				break;
			case BOOLEAN:
				value = String.valueOf(cell.getBooleanCellValue());
				break;
			case ERROR:
				value = ErrorEval.getText(cell.getErrorCellValue());
				break;
			default:
//				logger.log(POILogger.WARN,
//						"Unexpected cell cachedFormulaResultType (" + cell.getCachedFormulaResultTypeEnum() + ")");
				value = MyAbstractExcelConverter.EMPTY;
				break;
			}
			break;
		case BLANK:
			value = MyAbstractExcelConverter.EMPTY;
			break;
		case NUMERIC:
			value = _formatter.formatCellValue(cell);
			break;
		case BOOLEAN:
			value = String.valueOf(cell.getBooleanCellValue());
			break;
		case ERROR:
			value = ErrorEval.getText(cell.getErrorCellValue());
			break;
		default:
			System.err.println("Unexpected cell type (" + cell.getCellTypeEnum() + ")");
//			logger.log(POILogger.WARN, "Unexpected cell type (" + cell.getCellTypeEnum() + ")");
			return true;
		}

		final boolean noText = MyAbstractExcelConverter.isEmpty(value);
		final boolean wrapInDivs = !noText && isUseDivsToSpan() && !cellStyle.getWrapText();

		if (cellStyle.getIndex() != 0) {
			@SuppressWarnings("resource")
			XSSFWorkbook workbook = cell.getRow().getSheet().getWorkbook();
			String mainCssClass = getStyleClassName(workbook, cellStyle);

			if (wrapInDivs) {
				tableCellElement.setAttribute("class", mainCssClass + " " + cssClassContainerCell);
			} else {
				tableCellElement.setAttribute("class", mainCssClass);
			}

			if (noText) {
				/*
				 * if cell style is defined (like borders, etc.) but cell text is empty, add
				 * "&nbsp;" to output, so browser won't collapse and ignore cell
				 */
				value = "\u00A0";
			}
		}

		if (isOutputLeadingSpacesAsNonBreaking() && value.startsWith(" ")) {
			StringBuilder builder = new StringBuilder();
			for (int c = 0; c < value.length(); c++) {
				if (value.charAt(c) != ' ')
					break;
				builder.append('\u00a0');
			}

			if (value.length() != builder.length())
				builder.append(value.substring(builder.length()));

			value = builder.toString();
		}

		Text text = htmlDocumentFacade.createText(value);

		if (wrapInDivs) {
			Element outerDiv = htmlDocumentFacade.createBlock();
			outerDiv.setAttribute("class", this.cssClassContainerDiv);

			Element innerDiv = htmlDocumentFacade.createBlock();
			StringBuilder innerDivStyle = new StringBuilder();
			innerDivStyle.append("position:absolute;min-width:");
			innerDivStyle.append(normalWidthPx);
			innerDivStyle.append("px;");
			if (maxSpannedWidthPx != Integer.MAX_VALUE) {
				innerDivStyle.append("max-width:");
				innerDivStyle.append(maxSpannedWidthPx);
				innerDivStyle.append("px;");
			}
			innerDivStyle.append("overflow:hidden;max-height:");
			innerDivStyle.append(normalHeightPt);
			innerDivStyle.append("pt;white-space:nowrap;");
			ExcelToHtmlUtils.appendAlign(innerDivStyle, cellStyle.getAlignment());
			htmlDocumentFacade.addStyleClass(outerDiv, cssClassPrefixDiv, innerDivStyle.toString());

			innerDiv.appendChild(text);
			outerDiv.appendChild(innerDiv);
			tableCellElement.appendChild(outerDiv);
		} else {
			tableCellElement.appendChild(text);
		}

		return MyAbstractExcelConverter.isEmpty(value) && (cellStyle.getIndex() == 0);
	}

	protected String getStyleClassName(XSSFWorkbook workbook, XSSFCellStyle cellStyle) {
		final Short cellStyleKey = Short.valueOf(cellStyle.getIndex());

		String knownClass = excelStyleToClass.get(cellStyleKey);
		if (knownClass != null)
			return knownClass;

		String cssStyle = buildStyle(workbook, cellStyle);
		String cssClass = htmlDocumentFacade.getOrCreateCssClass(cssClassPrefixCell, cssStyle);
		excelStyleToClass.put(cellStyleKey, cssClass);
		return cssClass;
	}

	protected String buildStyle(XSSFWorkbook workbook, XSSFCellStyle cellStyle) {
		StringBuilder style = new StringBuilder();

		style.append("white-space:pre-wrap;");
		ExcelToHtmlUtils.appendAlign(style, cellStyle.getAlignment());

		switch (cellStyle.getFillPattern()) {
		// no fill
		case 0:
			break;
		case 1:
			final XSSFColor foregroundColor = cellStyle.getFillForegroundColorColor();
			if (foregroundColor == null)
				break;
			String fgCol = MyExcelToHtmlUtils.getColor(foregroundColor);
			style.append("background-color:" + fgCol + ";");
			break;
		default:
			final XSSFColor backgroundColor = cellStyle.getFillBackgroundColorColor();
			if (backgroundColor == null)
				break;
			String bgCol = MyExcelToHtmlUtils.getColor(backgroundColor);
			style.append("background-color:" + bgCol + ";");
			break;
		}

		buildStyle_border(workbook, style, "top", cellStyle.getBorderTopEnum(), cellStyle.getTopBorderColor());
		buildStyle_border(workbook, style, "right", cellStyle.getBorderRightEnum(), cellStyle.getRightBorderColor());
		buildStyle_border(workbook, style, "bottom", cellStyle.getBorderBottomEnum(), cellStyle.getBottomBorderColor());
		buildStyle_border(workbook, style, "left", cellStyle.getBorderLeftEnum(), cellStyle.getLeftBorderColor());

		XSSFFont font = cellStyle.getFont();
		buildStyle_font(workbook, style, font);

		return style.toString();
	}

	private void buildStyle_border(XSSFWorkbook workbook, StringBuilder style, String type, BorderStyle xlsBorder,
			short borderColor) {
		if (xlsBorder == BorderStyle.NONE) {
			return;
		}

		StringBuilder borderStyle = new StringBuilder();
		borderStyle.append(ExcelToHtmlUtils.getBorderWidth(xlsBorder));
		borderStyle.append(' ');
		borderStyle.append(ExcelToHtmlUtils.getBorderStyle(xlsBorder));

		java.awt.Color col = new java.awt.Color(borderColor);
		final XSSFColor color = new XSSFColor(col);
//		final XSSFColor color = workbook.getCustomPalette().getColor(borderColor);
		if (color != null) {
			borderStyle.append(' ');
			borderStyle.append(MyAbstractExcelUtils.getColor(color));
		}

		style.append("border-" + type + ":" + borderStyle + ";");
	}
	
	 protected boolean isTextEmpty( XSSFCell cell )
	    {
	        final String value;
	        switch ( cell.getCellTypeEnum() )
	        {
	        case STRING:
	            // XXX: enrich
	            value = cell.getRichStringCellValue().getString();
	            break;
	        case FORMULA:
	            switch ( cell.getCachedFormulaResultTypeEnum() )
	            {
	            case STRING:
	            	XSSFRichTextString str = cell.getRichStringCellValue();
	                if ( str == null || str.length() <= 0 )
	                    return false;

	                value = str.toString();
	                break;
	            case NUMERIC:
	                XSSFCellStyle style = cell.getCellStyle();
	                double nval = cell.getNumericCellValue();
	                short df = style.getDataFormat();
	                String dfs = style.getDataFormatString();
	                value = _formatter.formatRawCellContents(nval, df, dfs);
	                break;
	            case BOOLEAN:
	                value = String.valueOf( cell.getBooleanCellValue() );
	                break;
	            case ERROR:
	                value = ErrorEval.getText( cell.getErrorCellValue() );
	                break;
	            default:
	                value = MyExcelToHtmlUtils.EMPTY;
	                break;
	            }
	            break;
	        case BLANK:
	            value = MyExcelToHtmlUtils.EMPTY;
	            break;
	        case NUMERIC:
	            value = _formatter.formatCellValue( cell );
	            break;
	        case BOOLEAN:
	            value = String.valueOf( cell.getBooleanCellValue() );
	            break;
	        case ERROR:
	            value = ErrorEval.getText( cell.getErrorCellValue() );
	            break;
	        default:
	            return true;
	        }

	        return MyExcelToHtmlUtils.isEmpty( value );
	    }


}

package com.onlineitr.pdf;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.documents4j.api.IConverter;
import com.documents4j.job.LocalConverter;

public class RtfToPdf {

	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException {
		System.out.println(System.getProperty("java.io.tmpdir"));
		File inputFile = new File(System.getProperty("java.io.tmpdir") + File.separator + "itr");
		if (!inputFile.exists()) {
			boolean dir = inputFile.mkdir();
		}
		File outputFile = new File("D:/say_solution/documents/HelloWorld.pdf");
		IConverter converter = LocalConverter.builder().baseFolder(inputFile).workerPool(20, 25, 2, TimeUnit.SECONDS)
				.processTimeout(5, TimeUnit.SECONDS).build();

		Future<Boolean> conversion = converter.convert(new File("D:/say_solution/2019/1Mahi.rtf"))
				.as(com.documents4j.api.DocumentType.RTF).to(outputFile).as(com.documents4j.api.DocumentType.PDF)
				.prioritizeWith(1000).schedule();
		conversion.get();
	}

	public File convertRtfFileToPDF() {

		try {
			System.out.println(System.getProperty("java.io.tmpdir"));
			File inputFile = new File(System.getProperty("java.io.tmpdir") + File.separator + "itr");
			if (!inputFile.exists()) {
				boolean dir = inputFile.mkdir();
			}

			File outputFile = new File("D:/say_solution/documents/HelloWorld.pdf");
			IConverter converter = LocalConverter.builder().baseFolder(inputFile)
					.workerPool(20, 25, 2, TimeUnit.SECONDS).processTimeout(5, TimeUnit.SECONDS).build();

			Future<Boolean> conversion = converter.convert(new File("D:/say_solution/2019/1Mahi.rtf"))
					.as(com.documents4j.api.DocumentType.RTF).to(outputFile).as(com.documents4j.api.DocumentType.PDF)
					.prioritizeWith(1000).schedule();
			conversion.get();
			return outputFile;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

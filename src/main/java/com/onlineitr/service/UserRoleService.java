package com.onlineitr.service;

import com.onlineitr.model.UserRole;

public interface UserRoleService {

	public UserRole save(UserRole userRole);
}

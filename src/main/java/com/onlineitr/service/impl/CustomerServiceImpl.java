package com.onlineitr.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.onlineitr.exception.ApiResponse;
import com.onlineitr.exception.BadRequestException;
import com.onlineitr.exception.ConflictException;
import com.onlineitr.model.Documents;
import com.onlineitr.model.User;
import com.onlineitr.model.UserRole;
import com.onlineitr.payload.ChangePasswordDTO;
import com.onlineitr.payload.CustomerDTO;
import com.onlineitr.payload.LoginRequest;
import com.onlineitr.pdf.FileConversions;
import com.onlineitr.repository.DocumentRepository;
import com.onlineitr.repository.UserRepository;
import com.onlineitr.repository.UserRoleRepository;
import com.onlineitr.scheduler.StorageMonitor;
import com.onlineitr.service.CustomerService;
import com.onlineitr.utils.MethodUtils;
import com.onlineitr.utils.VariableUtils;
import com.opencsv.CSVReader;

@Service
public class CustomerServiceImpl implements CustomerService {

	private static final Logger logger = LogManager.getLogger(StorageMonitor.class);

	@Value("${customer.fileUpload.dir}")
	private String customerUploadFolder;

	@Value("${uploadDir.folder.path}")
	private String uploadDirFolderPath;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private DocumentRepository documentRepository;

	@Autowired
	private UserRoleRepository userRoleRepository;

	@Override
	public List<String> uploadCustomer(MultipartFile file) {

//		System.err.println(file.getOriginalFilename());

		if (file.isEmpty()) {
			logger.error("Sorry !! file is empty");
			throw new BadRequestException("Sorry !! file is empty", false);
		}

		List<String> listOfErrors = new ArrayList<String>();
		try {
			File convFile = new File(file.getOriginalFilename());
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();

			CSVReader reader = new CSVReader(new FileReader(convFile), ',', '"', 1);

			// Read all rows at once
			java.util.List<String[]> allRows = reader.readAll();

			UserRole userRole = userRoleRepository.findByName(VariableUtils.ROLE_USER).orElse(null);
//			System.err.println(userRole);
			if (userRole == null) {
				userRole = new UserRole();
				userRole.setName(VariableUtils.ROLE_USER);
				userRole = userRoleRepository.save(userRole);
			}

			// Read CSV line by line and use the string array as you want

			for (String[] row : allRows) {
//				System.err.println(row[0]);
				if (!MethodUtils.isObjectNullOrEmpty(row[0])) {
					String username = row[0];
					logger.info("User name ::" + username);

					String regex = "^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$";

					Pattern pattern = Pattern.compile(regex);
					Matcher matcher = pattern.matcher(username);

					if (!matcher.matches()) {
						listOfErrors.add(username + " Please enter valid Pancard no.");
						continue;
					}

					User customer = userRepository.findByUsername(username).orElse(null);
//					System.err.println(customer);
					if (customer != null) {
						listOfErrors.add(username + " already exist.");
						continue;
					}
					customer = new User(username, username, username);
					customer.setActiveStatus(false);
					customer.setUserActive(true);
					customer.setUserRole(userRole);
					userRepository.saveAndFlush(customer);

				}

			}
		} catch (IOException e) {
			e.printStackTrace();
//			throw new Exception("Something goes wrong");
		}

		return listOfErrors;

	}

	@Override
	public String uploadCustomerFiles(String year, MultipartFile zipFile) {

		Integer count = 0;
		File extractdirectory = new File(uploadDirFolderPath);
		if (!extractdirectory.exists()) {
			boolean isCreate = extractdirectory.mkdir();
			logger.info("Create Directory for file extraction ::  {} ", isCreate);
		}
		
		if (zipFile.isEmpty()) {
			logger.error("Zip file is empty");
			throw new BadRequestException("File Empty", false);
		}

		File destDir = new File(customerUploadFolder);
		if (!destDir.exists()) {
			boolean isCreate = destDir.mkdir();
			logger.info("Destionation directory created..");
		}

		logger.info("========== Extract start=================");
		boolean result = MethodUtils.extractZipFile(zipFile, extractdirectory);
		logger.info("==================File Extracted====================" + result);

		if (!result) {
			logger.error("Zip file not extracted properly.");
			throw new ConflictException("Zip not extracted properly.", false);
		}

		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy");
		File[] files = extractdirectory.listFiles();

		if (files.length > 0) {
			for (File file : files) {

				logger.info("======== Process files : {} ", file.getName());
				String originalFileName = file.getName();
				String fileNameWithoutExtension = originalFileName.substring(0, originalFileName.lastIndexOf("."));

				String[] splitFileName = fileNameWithoutExtension.split("_");
				String pancardNo = splitFileName[0];
				String username = splitFileName.length > 1 ? splitFileName[1] : null;
				User customer = userRepository.findByPanCardNo(pancardNo).orElse(null);
				if (customer != null) {
					try {
						UUID uuid = UUID.randomUUID();
						String randomUUIDString = uuid.toString();

						String customFileName = randomUUIDString
								+ originalFileName.substring(originalFileName.lastIndexOf("."));

						FileUtils.copyFile(file, new File(this.customerUploadFolder + customFileName));

						Documents documents = new Documents();
						documents.setUser(customer);
						documents.setUploadedFileName(customFileName);
						documents.setOriginalFileName(file.getName());
						documents.setPath(this.customerUploadFolder + customFileName);
						documents.setYear(year);

						documentRepository.saveAndFlush(documents);
						logger.info("Document uploaded , {} ", documents.getOriginalFileName());
						count++;

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				FileUtils.deleteDirectory(extractdirectory);
			} catch (IOException e) {
				e.printStackTrace();
			}
			logger.info("Extract dir deleted...");
		}

		return (count + " Files uploaded successfully");

	}

	@Override
	public User login(LoginRequest request) {

		if (MethodUtils.isObjectNullOrEmpty(request.getUsername())
				|| MethodUtils.isObjectNullOrEmpty(request.getPassword())) {
			throw new BadRequestException("Username and password must be required", true);
		}

		return userRepository.findByUsernameAndPasswordAndIsUserActiveTrue(request.getUsername(), request.getPassword())
				.orElse(null);
	}

	@Override
	public File customerMergeDocument(String customerId , String year) {

		System.err.println(customerId);
		String checkingFileName = customerUploadFolder + File.separator + customerId + "_" + year + ".pdf";
		File existingFile = new File(checkingFileName);
		boolean isExist = existingFile.exists();
		logger.info("File exist : {}" , isExist);
		
		if(isExist) {
			logger.info("Return with existing file name :  {} ",  existingFile.getAbsolutePath());
			return existingFile;
		}
		
		List<Documents> listOfDocument  = documentRepository.findAllByYearAndUser(year, new User(customerId));
		File downloadFile = null;
		if (MethodUtils.isListNullOrEmpty(listOfDocument)) {
			logger.info("Document not found for this year");
			throw new BadRequestException("Document not found for this year" ,false);
		}
		if (!MethodUtils.isListNullOrEmpty(listOfDocument)) {

			File tmpDir = new File(System.getProperty("java.io.tmpdir") + File.separator + "itr");
			if (!tmpDir.exists()) {
				boolean dir = tmpDir.mkdir();
			}

			List<File> listOfFiles = new ArrayList<File>();
			listOfDocument.forEach(doc -> {
//				System.err.println(doc.getPath());
				String extension = null;
				int i = doc.getOriginalFileName().lastIndexOf('.');
				if (i > 0) {
					extension = doc.getOriginalFileName().substring(i + 1);
				}

				if (MethodUtils.isObjectNullOrEmpty(extension)) {
					return;
				}

				File file;
				if ("rtf".equalsIgnoreCase(extension)) {
					file = FileConversions.convertRtfFileToPDF(doc.getUploadedFileName(), doc.getPath(),
							tmpDir.getAbsolutePath());
//					System.out.println("$$$$$$$$$$$$$$$$$$$$$$ :: " + file);
				} else if ("xlsx".equalsIgnoreCase(extension) || "xls".equalsIgnoreCase(extension)) {
					file = FileConversions.convertExcelToPDF(doc.getUploadedFileName(), doc.getPath(),
							tmpDir.getAbsolutePath());
//					System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@ :: " + file);
				} else if ("pdf".equalsIgnoreCase(extension)) {
					file = new File(doc.getPath());
				} else {
					return;
				}

				if (file != null) {
					listOfFiles.add(file);
				}
			});
			if (!MethodUtils.isListNullOrEmpty(listOfFiles)) {
				String outputFileName = customerUploadFolder + File.separator + listOfDocument.get(0).getUser().getId() + "_" + year + ".pdf";
				downloadFile = FileConversions.mergePDFDocuments(listOfFiles, outputFileName);
				
				try {
					FileUtils.copyFile(downloadFile, new File(this.customerUploadFolder + downloadFile.getName()));
				} catch (IOException e) {
					e.printStackTrace();
				}
//				System.err.println(downloadFile.getAbsolutePath());
			}
		}

		return downloadFile;
	}

	@Override
	public List<User> getAllCustomers() {
		return userRepository.getAllUserWithUserRole(VariableUtils.ROLE_USER);
	}

	@Override
	public User saveCusotmer(CustomerDTO customerDTO) {

		User customerFromDb = userRepository.findByPanCardNo(customerDTO.getPanCardNo()).orElse(null);

		if (customerFromDb != null) {
			throw new ConflictException("User already exist", true);
		}

		UserRole userRole = userRoleRepository.findByName(VariableUtils.ROLE_USER).orElse(null);
//		System.err.println(userRole);
		if (userRole == null) {
			userRole = new UserRole();
			userRole.setName(VariableUtils.ROLE_USER);
			userRole = userRoleRepository.save(userRole);
		}

		User customer = new User(customerDTO.getPanCardNo(), customerDTO.getPanCardNo(), customerDTO.getPanCardNo());
		customer.setActiveStatus(false);
		customer.setUserActive(true);
		customer.setUserRole(userRole);

		return userRepository.save(customer);
	}

	public String changePassword(ChangePasswordDTO input) {

		User customerFromDb = userRepository.findByPanCardNo(input.getPanCardNo()).orElse(null);

		if (customerFromDb == null) {
			throw new ConflictException("User does not exist.", true);
		}

		customerFromDb.setPassword(input.getPassword());
		userRepository.save(customerFromDb);

		return "Password updated success fully";
	}

	@Override
	public String changeAdminPassword(ChangePasswordDTO input) {

		User customerFromDb = userRepository.findById(input.getUserId()).orElse(null);

		if (customerFromDb == null) {
			throw new ConflictException("User does not exist.", true);
		}

		if ((input.getIsUserFirstTime() != null && !input.getIsUserFirstTime())
				&& !customerFromDb.getPassword().equals(input.getCurrentPassword())) {
			throw new ConflictException("Please enter valid current password.", true);
		}

		customerFromDb.setPassword(input.getPassword());
		userRepository.save(customerFromDb);

		return "Password updated success fully.";
	}

	@Override
	public Map<String, String> sendOTP(String mobile_no, String user_id) {

		User user = userRepository.findById(user_id).orElse(null);

		if (user == null) {
			throw new com.onlineitr.exception.EntityNotFoundException("User", false);
		}

		String opt = MethodUtils.randomAlphaNumeric(6);
		Map<String, String> mapOfResponse = sendOTPSms(mobile_no, opt);

		if (mapOfResponse.get("Status").equalsIgnoreCase("Success")) {
			user.setTempOtp(opt);
			user.getUserMobileNo().add(mobile_no);
			userRepository.saveAndFlush(user);
		}

		return mapOfResponse;
	}

	public Map<String, String> sendOTPSms(String mobileNo, String otp) {

		try {
			Map<String, String> map = new HashMap<String, String>();
			String USER_AGENT = "Mozilla/5.0";
			String url = "https://2factor.in/API/V1/" + VariableUtils.SMS_API_KEY + "/SMS/" + mobileNo + "/" + otp;

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			// add request header
			con.setRequestProperty("User-Agent", USER_AGENT);

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());

			map = VariableUtils.MAPPER.readValue(response.toString(), Map.class);
			return map;

		} catch (Exception e) {
			e.printStackTrace();
			Map<String, String> map = new HashMap<String, String>();
			map.put("Status", "Error");
			map.put("Details", "OTP does't send.Please try again!!");
			return map;
		}
	}

	@Override
	public ResponseEntity<ApiResponse> verifyOTP(String otp, String user_id) {

		User user = userRepository.findById(user_id).orElse(null);

		if (user == null) {
			throw new com.onlineitr.exception.EntityNotFoundException("User", false);
		}

		if (user.isActiveStatus() || user.getTempOtp() == null) {
			throw new ConflictException("User already activated", false);
		}
		if (otp.equalsIgnoreCase(user.getTempOtp())) {
			user.setActiveStatus(true);
			user.setTempOtp(null);
			userRepository.saveAndFlush(user);
			return new ResponseEntity<ApiResponse>(new ApiResponse(HttpStatus.OK.value(), "OTP matched!!", null, false),
					new HttpHeaders(), HttpStatus.OK);
		}

		return new ResponseEntity<ApiResponse>(
				new ApiResponse(HttpStatus.CONFLICT.value(), "OTP does't match. Please try again", null, false),
				new HttpHeaders(), HttpStatus.CONFLICT);
	}

	@Override
	public String changeUserBlockStaus(String userId, Boolean status) {

		User user = userRepository.findById(userId).orElse(null);

		if (user == null) {
			throw new com.onlineitr.exception.EntityNotFoundException("User", false);
		}

		user.setUserActive(status);
		userRepository.saveAndFlush(user);

		return "Change status successfully";
	}

	@Override
	public User getUserById(String id) {
		return userRepository.findById(id).orElse(null);
	}

	@Override
	public User updateUser(String id, CustomerDTO customer) {

		User user = userRepository.findById(id).orElse(null);

		if (user == null) {
			throw new com.onlineitr.exception.EntityNotFoundException("User", false);
		}

		user.setFirstName(customer.getFirstName());
		user.setLastName(customer.getLastName());
		user.setEmail(customer.getEmail());
		user.setAddress(customer.getAddress());
//		System.err.println(customer.getAadharCardNo());
		user.setAadharCardNo(customer.getAadharCardNo());
		user = userRepository.saveAndFlush(user);
		return user;
	}
	
	@Override
	public User createAdminUser() {
		
		UserRole userRoleAdmin = userRoleRepository.findByName(VariableUtils.ROLE_ADMIN).orElse(null);
		
		if(userRoleAdmin == null) {
			userRoleAdmin = new UserRole();
			userRoleAdmin.setName(VariableUtils.ROLE_ADMIN);
			userRoleAdmin = userRoleRepository.saveAndFlush(userRoleAdmin);
		}
		User adminUser = userRepository.findByUsernameAndUserRoleName("saysolution", VariableUtils.ROLE_ADMIN).orElse(null);
		
		if(adminUser != null) {
			throw new ConflictException("Admin user already exist", false);
		}
		
		adminUser = new User();
		adminUser.setFirstName("Say Solution");
		adminUser.setUsername("saysolution");
		adminUser.setPassword("saysolution");
		adminUser.setUserRole(userRoleAdmin);
		adminUser.setActiveStatus(true);
		adminUser.setUserActive(true);
		adminUser = userRepository.saveAndFlush(adminUser);
		return adminUser;
	}
}

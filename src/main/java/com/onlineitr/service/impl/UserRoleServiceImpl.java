package com.onlineitr.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlineitr.model.UserRole;
import com.onlineitr.repository.UserRoleRepository;
import com.onlineitr.service.UserRoleService;

@Service
public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
	private UserRoleRepository userRoleRepository;

	@Override
	public UserRole save(UserRole userRole) {
		return userRoleRepository.save(userRole);
	}
	

}

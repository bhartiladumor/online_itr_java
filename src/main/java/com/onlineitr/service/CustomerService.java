package com.onlineitr.service;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import com.onlineitr.exception.ApiResponse;
import com.onlineitr.model.User;
import com.onlineitr.payload.ChangePasswordDTO;
import com.onlineitr.payload.CustomerDTO;
import com.onlineitr.payload.LoginRequest;

public interface CustomerService {

	public List<String> uploadCustomer(MultipartFile file) ;

	public String uploadCustomerFiles(String year , MultipartFile file);

	public User login(LoginRequest request);
	
	public File customerMergeDocument(String customerId , String year);
	
	public List<User> getAllCustomers();
	
	public User saveCusotmer(CustomerDTO customerDTO);
	
	public String changePassword(ChangePasswordDTO input);
	
	public String changeAdminPassword(ChangePasswordDTO input);
	
	public Map<String , String> sendOTP(String mobile_no , String user_id);
	
	public ResponseEntity<ApiResponse> verifyOTP(String otp , String user_id);
	
	public String changeUserBlockStaus(String userId, Boolean status);
	
	public User getUserById(String id);
	
	public User updateUser(String id , CustomerDTO customer);
	
	public User createAdminUser();
}
